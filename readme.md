# Dual Band Collapsible Moxon Yagi

![](extended.jpg) ![](folded.jpg)

These are instructions for creating a foldable dual band Moxon Yagi hybrid antenna. I designed it with satellite work in mind, but it should be useful for anyone with a VHF/UHF radio and need for a beam antenna. On 2m it is a Moxon type, and there are two parasitic elements for 70cm. The feedpoint is on the Moxon which allows it to work on both bands without a diplexer. The moving pieces should be assembled with knurled nuts or wingnuts to make them easy to adjust by hand.

The NEC file I worked with for design and modeling is in this repository. I also included DXF files created in QCAD with the dimensions for the parts (cutlist.dxf) and an assembly diagram (assembled.dxf).

It provides about 6 dBi gain on VHF and 9 dBi on UHF.

*2m radiation pattern*

![](145880.png)

*70cm radiation pattern*

![](435350.png)

*VSWR as predicted by Xnex2c*

![](2mvswr.png) ![](70cmvswr.png)

As designed, the lowest SWR on UHF is a little high for satellite work so you may want to change the dimensions on your project. Let me know what you find!

# Assembly Instructions
## Materials
The cut measurements are in Metric to match the NEC model I started with, but I had to buy parts in America so hardware measurements are in Freedom units.

| Item                                         | Quantity      |
|----------------------------------------------|---------------|
| 1/8" x 1/2" aluminium bar stock              | 2.6m          |
| 1/8" thick plexiglass, lexan, or acrylic     | 30cm x 40cm   |
| 1" PVC Pipe                                  | 1/2 m         |
| #10 x 1/2" machine screws                    | 12            |
| #10 knurled nuts                             | 12            |
| #10 washers                                  | 4             |
| Long 1/8" pop rivets                         | 8             |
| #8 or #10 x 3/4" Self-tapping screws         | 4             |
| Coax cable (+1 terminator of your choice)    | 1m            |
| 3/16" I.D. ring terminals                    | 2             |

## Cut List
### Aluminium Bar Stock Lengths
| Label    | Length    | Quantity    |
|----------|-----------|-------------|
| A1       | 349mm     | 4 pieces    |
| A2       | 106mm     | 2 pieces    |
| A3       | 148mm     | 2 pieces    |
| A4       | 310mm     | 1 piece     |
| A5       | 330mm     | 1 piece     |
| A6       | 30mm      | 1 piece     |

### Plastic dimensions
| Label    | Dimensions      | Quantity    |
|----------|-----------------|-------------|
| P1       | 145mm x 40mm    | 1 piece     |
| P2       | 60mm x 40mm     | 1 piece     |
| P3       | 20mm x 100mm    | 2 pieces    |

The PVC pipe will be a handle, so I recommend putting the rest of the project together and cutting the pipe to a comfortable length.

## Instructions
1. Cut the aluminium and plastic pieces to the dimensions in the cut list. It will be helpful to label them with the names in the list.
2. Drill the pictured holes in the aluminium pieces. If you are using #10 machine screws 3/16" is a good size for the machine screw holes. If  you use 1/8" pop rivets drill 1/8" holes for them.
    * **A1** - All holes are for machine screws. ***To make the antenna fold nicely you will also need to use a file to round off one end of each A1 piece.***

      ![](A1.png)

    * **A2** - The left hole is for a machine screw, the two right holes are for pop rivets

      ![](A2.png)

    * **A3** - The left hole is for a machine screw, the two right holes are for pop rivets

      ![](A3.png)

    * **A4** - All holes are for machine screws

      ![](A4.png)

    * **A5** - All holes are for machine screws

      ![](A5.png)

    * **A6** - All holes are for machine screws

      ![](A6.png)


3. Drill the pictured holes in the plastic pieces. Adjust the diameter of the holes if you are using different sized screws or rivets. (Sidenote: My grandfather was a draftsman who helped design the Titan II rocket. I am not a draftsman.)
    * **P1** - The pairs of holes are for machine screws, the centered holes are for self-tapping screws

      ![](P1.png)

    * **P2** - The pairs of holes are for machine screws, the centered holes are for self-tapping screws

      ![](P2.png)

    * **P3** - All holes are for pop rivets

      ![](P3.png)


4. Drill pilot holes for your self-tapping screws at the following lengths on your PVC pipe:

      ![](pvc.png)

5. Use pop rivets to connect the three side pieces of the 2m Moxon portion of the antenna as shown (A2, A3, P3). You should have two of these assemblies.

      ![](assembly.png)

6. Put #10 machine screws through the holes shown below on P1 and P2 and secure their heads with tape to keep them from falling out
    * P1

      ![](6p1.png)

    * P2

      ![](6p2.png)

7. Using self-tapping screws, connect P1 and P2 to the PVC pipe boom so that the threaded ends of the machine screws point away from the boom

    ![](step7.png)

8. Separate the center conductor and shield of the unterminated end of the coax cable, and crimp the ring terminals to them
9. Feed the ring terminal end of the coax cable through the boom so that it comes out the front (that's the non-squiggly end in the diagrams).
10. Connect A4 to the first set of screws on P1 and secure with knurled nuts
11. Connect A5 to the third set of screws on P1 and secure with knurled nuts
12. Put the ring terminals over the second set of screws on P1
13. Connect 2 A1 pieces of aluminium to the second set of screws on P1 (on top of the ring termnials from Step 12) and secure with knurled nuts
14. Connect 2 A1 pieces of aluminium to the set of screws on P2 
15. Place A6 over the set of screws on P2 (on top of the A1 bars from Step 14) and secure with knurled nuts
16. Connect the assemblies from Step 5 to the free ends and secure with knurled nuts

In the end, you should have all pieces assembled like this:

![](complete.png)

Your feed point is in between the top two A1 pieces. You can either let the coax run back down the middle of the assembled antenna or loop it over the front 70cm element and through the boom.

Now that everything is assembled, you can remove the tape from the plastic pieces and put a dot of superglue on the machine screw heads to keep them from turning when you tighten the knurled nuts.

I hope these instructions were helpful. Enjoy your portable UHF/VHF beam!
